import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ProfileComponent } from './profile/profile.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from './services/auth.guard.service';
import { NotAuthGuard } from './services/notauth.guard.service.';

import { LoginComponent } from './auth/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { PickTeamComponent } from './profile/pick-team/pick-team.component';

const appRoutes: Routes = [
    { path: '', component: HomepageComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent, canActivate: [NotAuthGuard] },
    { 
        path: 'profile', 
        component: ProfileComponent, 
        canActivate: [AuthGuard],
        children: [
            { 
                path: 'pick-team', 
                component: PickTeamComponent
            }
        ] 
    },
    { path: 'admin', component: AdminPanelComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}