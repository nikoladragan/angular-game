import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { MissionsComponent } from './missions/missions.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AppRoutingModule } from './app.routing.service';
import { ProfileHeaderComponent } from './profile/profile-header/profile-header.component';
import { GlobalService } from './services/global.service';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './team/new-team/new-team.component';
import { ConsumablesService } from './services/consumables.service';
import { CharactersService } from './services/characters.service';
import { SpellsService } from './services/spells.service';
import { AdminComponent } from './admin/admin.component';
import { AddComponent } from './admin/add/add.component';
import { RemoveComponent } from './admin/remove/remove.component';
import { CharacterComponent } from './admin/add/character/character.component';
import { SpellComponent } from './admin/add/spell/spell.component';
import { ConsumableComponent } from './admin/add/consumable/consumable.component';
import { MyTeamsComponent } from './team/my-teams/my-teams.component';
import { TeamService } from './services/team.service';
import { AuthService } from './services/auth.service';
import { ProfileService } from './services/profile.service';

var firebaseConfig = {
	apiKey: "AIzaSyD4QcXYXVaDW6ege7US9V7OHVg8QX44A5s",
	authDomain: "angulargame-805f2.firebaseapp.com",
	databaseURL: "https://angulargame-805f2.firebaseio.com",
	projectId: "angulargame-805f2",
	storageBucket: "angulargame-805f2.appspot.com",
	messagingSenderId: "454895091808"
};

@NgModule({
	declarations: [
		AppComponent,
		AuthComponent,
		HeaderComponent,
		ProfileComponent,
		MissionsComponent,
		HomepageComponent,
		ProfileHeaderComponent,
		TeamComponent,
		NewTeamComponent,
		AdminComponent,
		AddComponent,
		RemoveComponent,
		CharacterComponent,
		SpellComponent,
		ConsumableComponent,
		MyTeamsComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFirestoreModule
	],
	providers: [
		GlobalService,
		CharactersService,
		SpellsService,
		ConsumablesService,
		TeamService,
		AuthService,
		ProfileService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
