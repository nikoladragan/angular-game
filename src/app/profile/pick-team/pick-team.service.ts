import { Injectable } from '@angular/core';

@Injectable()
export class PickTeamService {
	collection = [];
	pickedElements = [];
	activeSlot = -1;
	selectedSpells = {
		0: {
			length: 0,
			spells: []
		},
		1: {
			length: 0,
			spells: []
		},
		2: {
			length: 0,
			spells: []
		},
	};

	team: any[] = [];

	constructor() {
	}
}
