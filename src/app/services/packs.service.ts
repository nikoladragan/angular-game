import { Injectable, Output, IterableDiffers } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';


interface Packs {
	tutorial: Object;
	basic: Object;
}

interface Profile {
	finishedTutorial?: boolean,
	displayName?: string,
	spellCollection?: string[],
	characterCollection?: string[],
	consumableCollection?: string[],
	gold?: number;
}
interface Spell {
	amount: number,
	description: string,
	duration: number,
	name: string,
	spellGroup: string,
	type: string,
	rarity: string
}
interface Character {
	name: string,
	description: string,
	type: string
}
interface Consumable {
	name: string,
	description: string,
	type: string
}

@Injectable()
export class PacksService implements Packs {
	// pack objects
	tutorial = {
		amountMin: 10,
		amountMax: 10,
		characters: 100,
		spells: 100,
		consumable: 100,
		gold: 90
	}
	basic = {
		amountMin: 3,
		amountMax: 7,
		characters: 30,
		spells: 30,
		consumable: 60,
		gold: 10
	}

	// profile variables
	profileDoc: AngularFirestoreDocument<Profile>;
	profile: Observable<Profile>;

	// spell variables
	spellsCollection: AngularFirestoreCollection<Spell>;
	spells: Observable<Spell[]>;
	spellsObject: any;
	spellsSubscribe;

	// character variables
	charactersCollection: AngularFirestoreCollection<Character>;
	characters: Observable<Character[]>;
	charactersObject: any;
	charactersSubscribe;

	// consumable variables
	consumablesCollection: AngularFirestoreCollection<Consumable>;
	consumables: Observable<Consumable[]>;
	consumablesObject: any;
	consumablesSubscribe;

	rewardPack = [];

	differ;
	numberOfRewards;
	rewardClaimed: boolean = false;

	playerData;

	constructor(
		private authService: AuthService,
		private afs: AngularFirestore,
		private differs: IterableDiffers
	) {
		this.profileDoc = this.afs.doc('profiles/' + authService.userId);
		this.profile = this.profileDoc.valueChanges();
		this.differ = differs.find([]).create(null);

		this.profile.subscribe(data => {
			this.playerData = data;
			console.log(this.playerData);
			
		});

		this.spellsCollection = this.afs.collection('spells', ref => ref);
		this.spells = this.spellsCollection.snapshotChanges().map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as Spell;
				const id = a.payload.doc.id;
				const cardType = 'spell';

				return { id, cardType, ...data };
			});
		});
		this.spellsSubscribe = this.spells.subscribe(data => {
			this.spellsObject = data;
		});

		this.charactersCollection = this.afs.collection('characters', ref => ref);
		this.characters = this.charactersCollection.snapshotChanges().map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as Character;
				const id = a.payload.doc.id;
				const cardType = 'character';
				
				return { id, cardType, ...data };
			});
		});
		this.charactersSubscribe = this.characters.subscribe(data => {
			this.charactersObject = data;
		});

		this.consumablesCollection = this.afs.collection('consumables', ref => ref);
		this.consumables = this.consumablesCollection.snapshotChanges().map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as Consumable;
				const id = a.payload.doc.id;
				const cardType = 'consumable';

				return { id, cardType, ...data };
			});
		});
		this.consumablesSubscribe = this.consumables.subscribe(data => {
			this.consumablesObject = data;
		});		
	}

	getGold(gold, percentage) {
		var roundGold = 100;
		var increment: number = 0;
		while (roundGold > (100 - percentage)) {
			roundGold = Math.floor((Math.random() * 100) + 1);
			increment++;
		}

		return gold * increment;
	}

	getRandom(x) {
		return Math.floor(Math.random() * x);
	}

	doCheck() {
		var changes = this.differ.diff(this.rewardPack); // check for changes

		if (changes && this.rewardPack.length === this.numberOfRewards) {
			console.log('usao');
			let gold = this.getGold(100, this.tutorial.gold);
			if (this.playerData.gold != undefined) {
				gold += this.playerData.gold;
			}

			this.spellsSubscribe.unsubscribe();
			this.charactersSubscribe.unsubscribe();
			this.consumablesSubscribe.unsubscribe();

			let spellsList = [];
			let charactersList = [];
			let consumablesList = [];
			this.rewardPack.filter(
				data => {
					switch (data.cardType) {
						case 'spell':
							spellsList.push(data.id);
							break;
						case 'character':
							charactersList.push(data.id);
							break;
						case 'consumable':
							consumablesList.push(data.id);
							break;
						default:
							return null;
					}
				}
			);

			if (this.playerData.characterCollection != undefined) {
				let phArray = this.playerData.characterCollection;
				phArray = phArray.concat(charactersList);
				charactersList = phArray;
			}

			if (this.playerData.spellCollection != undefined) {
				let phArray = this.playerData.spellCollection;
				phArray = phArray.concat(spellsList);
				spellsList = phArray;
			}

			if (this.playerData.consumableCollection != undefined) {
				let phArray = this.playerData.consumableCollection;
				phArray = phArray.concat(consumablesList);
				consumablesList = phArray;
			}


			this.profileDoc.update({
				spellCollection: spellsList,
				characterCollection: charactersList,
				consumableCollection: consumablesList,
				gold: gold
			});
		}
	}

	updateProfile(object) {
		this.profileDoc.update(object);
	}

	openPack(type: string) {
		if (!this.rewardClaimed) {
			console.log('Pack opened');
			// this.rewardClaimed = true;
			this.rewardPack = [];

			var max;
			var arrayOfItems: any[] = [];

			const pack = this[type];
			console.log(pack);
			
			this.numberOfRewards = pack.amountMin;

			for (var i = pack.amountMin; i <= pack.amountMax; i++) {
				var number = Math.random();
				if (number < 0.5) {
					this.numberOfRewards = i;
					break;
				}
			}
			for (var i: any = 0; i < this.numberOfRewards; i++) {
				var potentialRewards = [
					{
						name: 'characters',
						value: this.getRandom(pack.characters)
					},
					{
						name: 'spells',
						value: this.getRandom(pack.spells)
					},
					{
						name: 'consumables',
						value: this.getRandom(pack.consumable)
					}
				];
				max = potentialRewards[0];

				for (var l = 1; l < potentialRewards.length; l++) {
					if (potentialRewards[l].value > max.value) {
						max = potentialRewards[l];
					}
				}
				arrayOfItems.push(max.name);
			}
			for (var i: any = 0; i < arrayOfItems.length; i++) {
				let itemId;
				switch (arrayOfItems[i]) {
					case 'spells':
						itemId = this.getRandom(this.spellsObject.length - 1);
						this.rewardPack.push(this.spellsObject[itemId]);
						break;
					case 'characters':
						itemId = this.getRandom(this.charactersObject.length - 1);
						this.rewardPack.push(this.charactersObject[itemId]);
						break;
					case 'consumables':
						itemId = this.getRandom(this.consumablesObject.length - 1);
						this.rewardPack.push(this.consumablesObject[itemId]);
						break;
					default:
						return null;
				}
			}
		} else {
			console.log('no no');
		}
	}
}
