import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { AuthService } from './services/auth.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	constructor(
		private auth: AuthService
	) {}

	ngOnInit() {
		const id = localStorage.getItem('id');
		
		if(id) {
			this.auth.userId = id;
		}
	}
}
