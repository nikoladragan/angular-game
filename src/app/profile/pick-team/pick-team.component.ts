import { Component, OnInit, Input } from '@angular/core';
import { PickTeamService } from './pick-team.service';

@Component({
	selector: 'app-pick-team',
	templateUrl: './pick-team.component.html',
	styleUrls: ['./pick-team.component.scss']
})
export class PickTeamComponent implements OnInit {
	Arr = Array;
	constructor(
		private pickTeam: PickTeamService
	) { }

	ngOnInit() {
		
	}

	onDelete(i: number) {
		console.log(i);

		this.pickTeam.team.splice(i, 1);
		this.pickTeam.pickedElements[i].classList.remove('added');
		this.pickTeam.pickedElements.splice(i, 1);

		this.pickTeam.activeSlot = -1;
	}

	onMakeActive(i, event) {
		this.pickTeam.activeSlot = i;
	}

	addTeam() {
		console.log(this.pickTeam.team);
	}
}
