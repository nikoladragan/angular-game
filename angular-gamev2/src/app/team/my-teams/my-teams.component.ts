import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../services/team.service';

@Component({
	selector: 'app-my-teams',
	templateUrl: './my-teams.component.html',
	styleUrls: ['./my-teams.component.scss']
})
export class MyTeamsComponent implements OnInit {

	constructor(
		private team: TeamService
	) { }

	ngOnInit() {
	}

}
