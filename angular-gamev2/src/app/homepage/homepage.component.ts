import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';

@Component({
	selector: 'app-homepage',
	templateUrl: './homepage.component.html',
	styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

	constructor(
		private profile: ProfileService
	) { }

	ngOnInit() {
	}
}
