import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { log } from 'util';

interface Rarity {
	traits: number;
}

@Component({
	selector: 'app-spell',
	templateUrl: './spell.component.html',
	styleUrls: ['./spell.component.scss']
})
export class SpellComponent implements OnInit {
	collection: AngularFirestoreCollection<any>;
	rarityData: any[];
	rarityCol: AngularFirestoreCollection<Rarity>;
	rarity: any;

	constructor(
		private afs: AngularFirestore
	) {
		this.rarityCol = this.afs.collection<Rarity>('rarity');
		this.rarity = this.rarityCol.snapshotChanges().map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as Rarity;
				console.log(data);
				
				return { data };
			})
		});
	}

	ngOnInit() {
	}

	onSubmit(form: NgForm) {
		const f = form.value;
		console.log(f);
		console.log(this.rarityCol);
		console.log(this.rarity);
	}

}
