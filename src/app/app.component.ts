import { AuthService } from './services/auth.service';
import { Component, OnInit } from '@angular/core';
// import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';


// interface Note {
// 	content: string;
// 	hearts: number;
// 	id?: string;
// }

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	// notesCollection: AngularFirestoreCollection<Note>;
	// notes: Observable<Note[]>;

	constructor(
		private authService: AuthService,
		// private asf: AngularFirestore
	) {}

	ngOnInit() {
		const id = localStorage.getItem('id');

		if(id) {
			this.authService.userId = id;
		}
		// this.notesCollection = this.asf.collection('notes', ref => {
			// return ref.orderBy('hearts').limit(10);
		// });
		// this.notes = this.notesCollection.valueChanges();
		// this.notesCollection.add({content: 'test', hearts: 3, id: '123'});
		// this.notesCollection.doc('RswMTnQM36lK1tbbt666').set({hearts: 1231, content: 'ma daj', id: 'nov id'});

		
	} 

}
