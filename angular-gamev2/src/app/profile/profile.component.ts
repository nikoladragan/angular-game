import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

	constructor(
		private global: GlobalService
	) { }

	ngOnInit() {
		// this.global.route = 'Profile';
	}

}
