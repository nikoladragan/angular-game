import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';

@Component({
	selector: 'app-missions',
	templateUrl: './missions.component.html',
	styleUrls: ['./missions.component.scss']
})
export class MissionsComponent implements OnInit {

	constructor(
		private global: GlobalService
	) { }

	ngOnInit() {
		// this.global.route = 'Missions';
	}

}
