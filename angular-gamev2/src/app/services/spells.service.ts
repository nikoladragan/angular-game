import { Injectable } from '@angular/core';

@Injectable()
export class SpellsService {
	spells = [
		{
			name: 'spell 1',
			desc: 'desc 1',
		},
		{
			name: 'spell 2',
			desc: 'desc 2',
		},
		{
			name: 'spell 3',
			desc: 'desc 3',
		},
		{
			name: 'spell 4',
			desc: 'desc 4',
		},
		{
			name: 'spell 5',
			desc: 'desc 5',
		},
		{
			name: 'spell 6',
			desc: 'desc 6',
		},
		{
			name: 'spell 7',
			desc: 'desc 7',
		},
		{
			name: 'spell 8',
			desc: 'desc 8',
		},
		{
			name: 'spell 9',
			desc: 'desc 9',
		},
		{
			name: 'spell 1',
			desc: 'desc 1',
		},
		{
			name: 'spell 10',
			desc: 'desc 10',
		},
		{
			name: 'spell 11',
			desc: 'desc 11',
		},
		{
			name: 'spell 12',
			desc: 'desc 12',
		},
		{
			name: 'spell 13',
			desc: 'desc 13',
		},
		{
			name: 'spell 14',
			desc: 'desc 14',
		},
		{
			name: 'spell 15',
			desc: 'desc 15',
		},
		{
			name: 'spell 16',
			desc: 'desc 16',
		},
		{
			name: 'spell 17',
			desc: 'desc 17',
		},
		{
			name: 'spell 18',
			desc: 'desc 18',
		},
		{
			name: 'spell 19',
			desc: 'desc 19',
		},
		{
			name: 'spell 20',
			desc: 'desc 20',
		},
	]
	constructor() { }
}
