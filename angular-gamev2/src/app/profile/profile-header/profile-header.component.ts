import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-profile-header',
	templateUrl: './profile-header.component.html',
	styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit {
	constructor(
		private global: GlobalService,
		private auth: AuthService
	) {

	}

	ngOnInit() {
	}
}
