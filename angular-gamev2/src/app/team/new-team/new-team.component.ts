import { Component, OnInit } from '@angular/core';
import { CharactersService } from '../../services/characters.service';
import { ConsumablesService } from '../../services/consumables.service';
import { SpellsService } from '../../services/spells.service';
import { TeamService } from '../../services/team.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-new-team',
	templateUrl: './new-team.component.html',
	styleUrls: ['./new-team.component.scss']
})
export class NewTeamComponent implements OnInit {
	teamSlots = 3;
	Arr = Array;
	activeSlot: number;
	picks = [
		{
			consumables: true, // false,
			character: true, // false,
			spells: true // false,
		},
		{
			consumables: true, // false,
			character: true, // false,
			spells: true // false,
		},
		{
			consumables: true, // false,
			character: true, // false,
			spells: true // false,
		}
	];
	showList: number = 0;
	pickedTeam = [
		{
			character: {},
			spells: [],
			consumables: []
		},
		{
			character: {},
			spells: [],
			consumables: []
		},
		{
			character: {},
			spells: [],
			consumables: []
		}
	];
	

	constructor(
		private char: CharactersService,
		private spell: SpellsService,
		private cons: ConsumablesService,
		private team: TeamService,
		private router: Router,
	) { }

	ngOnInit() {
	}
	addNewSlot() {
		if (this.teamSlots < 3) {
			this.teamSlots++;
		}
	}
	deleteSlot() {
		if (this.teamSlots > 0) {
			this.teamSlots--;
			this.activeSlot = null; // should be enhanced
		}
	}
	saveTeam() {
		console.log(this.pickedTeam);
		this.team.team.push(this.pickedTeam);
		this.router.navigate(['team', 'my-teams']);
	}
	changeList(i) {
		this.showList = i;
		console.log(i);
	}

	addCharacter(i) {
		if(this.activeSlot !== undefined && Object.keys(this.pickedTeam[this.activeSlot].character).length === 0 && this.pickedTeam[this.activeSlot].character.constructor === Object) {
		// if(this.activeSlot !== undefined && this.pickedTeam[this.activeSlot].character === 0) {
			this.pickedTeam[this.activeSlot].character = this.char.characters[i];
			this.char.characters.splice(i, 1);
		}
	}
	addSpell(i) {
		if(this.pickedTeam[this.activeSlot].spells.length < 3) {
			this.pickedTeam[this.activeSlot].spells.push(this.spell.spells[i]);
			this.spell.spells.splice(i, 1);
		}
	}
	addConsumable(i) {
		if(this.pickedTeam[this.activeSlot].consumables.length < 4) {
			this.pickedTeam[this.activeSlot].consumables.push(this.cons.consumables[i]);
			this.cons.consumables.splice(i, 1);
		}
	}
	deleteChar(i) {
		this.char.characters.push(this.pickedTeam[i].character);
		console.log(this.pickedTeam[i].character);
		this.pickedTeam[i].character = {};
	}
	deleteSpell(i, j) {
		let deleted = this.pickedTeam[i].spells.splice(j, 1);
		this.spell.spells.push(deleted[0]);
	}
	deleteConsumable(i, j) {
		let deleted = this.pickedTeam[i].consumables.splice(j, 1);
		this.cons.consumables.push(deleted[0]);
	}
}
