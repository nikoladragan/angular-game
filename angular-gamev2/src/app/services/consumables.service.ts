import { Injectable } from '@angular/core';

@Injectable()
export class ConsumablesService {
	consumables = [
		{
			name: 'consumable 1',
			desc: 'desc 1',
		},
		{
			name: 'consumable 2',
			desc: 'desc 2',
		},
		{
			name: 'consumable 3',
			desc: 'desc 3',
		},
		{
			name: 'consumable 4',
			desc: 'desc 4',
		},
		{
			name: 'consumable 5',
			desc: 'desc 5',
		},
		{
			name: 'consumable 6',
			desc: 'desc 6',
		},
		{
			name: 'consumable 7',
			desc: 'desc 7',
		},
		{
			name: 'consumable 8',
			desc: 'desc 8',
		},
		{
			name: 'consumable 9',
			desc: 'desc 9',
		},
		{
			name: 'consumable 1',
			desc: 'desc 1',
		},
		{
			name: 'consumable 10',
			desc: 'desc 10',
		},
		{
			name: 'consumable 11',
			desc: 'desc 11',
		},
		{
			name: 'consumable 12',
			desc: 'desc 12',
		},
		{
			name: 'consumable 13',
			desc: 'desc 13',
		},
		{
			name: 'consumable 14',
			desc: 'desc 14',
		},
		{
			name: 'consumable 15',
			desc: 'desc 15',
		},
		{
			name: 'consumable 16',
			desc: 'desc 16',
		},
		{
			name: 'consumable 17',
			desc: 'desc 17',
		},
		{
			name: 'consumable 18',
			desc: 'desc 18',
		},
		{
			name: 'consumable 19',
			desc: 'desc 19',
		},
		{
			name: 'consumable 20',
			desc: 'desc 20',
		},
	]
	constructor() { }

}
