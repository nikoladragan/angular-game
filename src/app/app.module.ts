import { AuthGuard } from './services/auth.guard.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore'

import { AuthService } from './services/auth.service';
import { AppRoutingModule } from './app.routing.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { FormsModule } from '@angular/forms';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { PacksService } from './services/packs.service';
import { NotAuthGuard } from './services/notauth.guard.service.';
import { PickTeamComponent } from './profile/pick-team/pick-team.component';
import { PickTeamService } from './profile/pick-team/pick-team.service';

var firebaseConfig = {
    apiKey: "AIzaSyD4QcXYXVaDW6ege7US9V7OHVg8QX44A5s",
    authDomain: "angulargame-805f2.firebaseapp.com",
    databaseURL: "https://angulargame-805f2.firebaseio.com",
    projectId: "angulargame-805f2",
    storageBucket: "angulargame-805f2.appspot.com",
    messagingSenderId: "454895091808"
};

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		HomepageComponent,
		HeaderComponent,
		ProfileComponent,
		TutorialComponent,
		AdminPanelComponent,
		PickTeamComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFirestoreModule
	],
	providers: [
		AuthService,
		AuthGuard,
		NotAuthGuard,
		PacksService,
		PickTeamService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
