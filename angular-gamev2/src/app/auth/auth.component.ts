import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
	isLogin: boolean = true;
	
	constructor(
		private authService: AuthService
	) { }

	ngOnInit() {
	}

	checkboxClicked(event) {
		if(event.target.checked) {
			console.log('checked');
			this.isLogin = false;
		} else {
			console.log('unchecked');
			this.isLogin = true;
		}
	}

	onSubmit(form: NgForm) {
		if(form.valid) {
			console.log(form.value);
			if(this.isLogin) {
				this.authService.loginUser(form.value.email, form.value.password);
			} else {
				this.authService.registerUser(form.value.email, form.value.password);
			}
		} else {
			console.log('no no, stop hacking');
		}
	}
}
