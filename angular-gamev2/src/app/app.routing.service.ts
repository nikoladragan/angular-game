import { ConsumableComponent } from './admin/add/consumable/consumable.component';
import { SpellComponent } from './admin/add/spell/spell.component';
import { CharacterComponent } from './admin/add/character/character.component';
import { RemoveComponent } from './admin/remove/remove.component';
import { AddComponent } from './admin/add/add.component';
import { AdminComponent } from './admin/admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ProfileComponent } from './profile/profile.component';
import { MissionsComponent } from './missions/missions.component';
import { AuthComponent } from './auth/auth.component';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './team/new-team/new-team.component';
import { MyTeamsComponent } from './team/my-teams/my-teams.component';

const appRoutes: Routes = [
	{ path: '', component: HomepageComponent, pathMatch: 'full' },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'missions', component: MissionsComponent },
	{ path: 'login', component: AuthComponent },
	{ 
		path: 'team', 
		component: TeamComponent, 
		children: [
			{
				path: 'new-team',
				component: NewTeamComponent
			},
			{
				path: 'my-teams',
				component: MyTeamsComponent
			}
		]
	},
	{ 
		path: 'admin', 
		component: AdminComponent,
		children: [
			{
				path: 'add',
				component: AddComponent,
				children: [
					{
						path: 'character',
						component: CharacterComponent
					},
					{
						path: 'spell',
						component: SpellComponent
					},
					{
						path: 'consumable',
						component: ConsumableComponent
					}
				]
			},
			{
				path: 'remove',
				component: RemoveComponent
			}
		]
	}
]

@NgModule({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})

export class AppRoutingModule {

}