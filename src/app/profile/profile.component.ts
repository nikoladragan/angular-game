import { PacksService } from './../services/packs.service';
import { NgForm } from '@angular/forms';
import { AuthService } from './../services/auth.service';
import { Component, OnInit, HostListener } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

import { PickTeamService } from './pick-team/pick-team.service';


interface Profile {
	finishedTutorial: boolean,
	displayName: string,
	spellCollection: string[],
	characterCollection: string[],
	consumableCollection: string[],
	gold: number;
}
interface ProfileId extends Profile {
	id: string;
}

interface Spell {
	amount: number,
	description: string,
	duration: number,
	icon: string,
	id: string,
	name: string,
	spellGroup: string,
	type: string
}

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	profileCol: AngularFirestoreCollection<Profile>;
	profileDoc: AngularFirestoreDocument<Profile>;
	profile: Observable<Profile>;
	profileSnapshot: any;

	spellsIHave: any[] = [];
	spellIdsIHave: string[] = [];

	charactersIHave: any[] = [];
	charactersIdsIHave: string[] = [];

	consumablesIHave: any[] = [];
	consumablesIdsIHave: string[] = [];

	gold: number = 0;

	constructor(
		private authService: AuthService,
		private afs: AngularFirestore,
		private pickTeam: PickTeamService,
		private packs: PacksService
	) {
		// console.log('profile constructor');
		if (authService.userId) {
			this.profileDoc = this.afs.doc('profiles/' + authService.userId);
			this.profile = this.profileDoc.valueChanges();
			this.profile.subscribe(data => {
				this.spellsIHave = [];
				this.charactersIHave = [];
				this.consumablesIHave = []
				if (data.spellCollection) {
					for (let i = 0; i < data.spellCollection.length; i++) {
						const id = data.spellCollection[i];
						this.spellIdsIHave.push(id);
						const spell = this.afs.doc('spells/' + id);
						const spellObs = spell.valueChanges();
						spellObs.subscribe(data => {
							let phData: any = {}
							phData = data;
							phData.id = id;

							this.spellsIHave.push(phData);
						});
					}
				}
				if (data.characterCollection) {
					for (let i = 0; i < data.characterCollection.length; i++) {
						const id = data.characterCollection[i];
						this.charactersIdsIHave.push(id);
						const character = this.afs.doc('characters/' + id);
						const characterObs = character.valueChanges();
						characterObs.subscribe(data => {
							let phData: any = {}
							phData = data;
							phData.id = id;

							this.charactersIHave.push(phData);
						});
					}
				}
				if (data.consumableCollection) {
					for (let i = 0; i < data.consumableCollection.length; i++) {
						const id = data.consumableCollection[i];
						this.consumablesIdsIHave.push(id);
						const consumable = this.afs.doc('consumables/' + id);
						const consumableObs = consumable.valueChanges();
						consumableObs.subscribe(data => {
							let phData: any = {}
							phData = data;
							phData.id = id;

							this.consumablesIHave.push(phData);
						});
					}
				}

				this.gold = data.gold;
			});

			this.profileCol = this.afs.collection<Profile>('profiles');
			this.profileSnapshot = this.profileCol.snapshotChanges().map(actions => {
				return actions.map(a => {
					const data = a.payload.doc.data() as Profile;
					const id = a.payload.doc.id;

					return { id, ...data };
				})
			})
		}
	}

	ngOnInit() { }

	onSubmit(form: NgForm) {
		const newDisplayName = form.value.displayName;
		this.profileDoc.update({ displayName: newDisplayName });
	}

	getPost(postId) {
		this.profileDoc = this.afs.doc('profiles/' + postId);
	}

	getCharacter(item, event) {
		if (this.pickTeam.pickedElements.length === 0) {
			console.log('first');
			push(this);
		} else {
			let found = false;
			for (let i = 0; i < this.pickTeam.pickedElements.length; i++) {
				if (event.srcElement === this.pickTeam.pickedElements[i]) {
					found = true;
					break;
				}
			}

			if (found) {
				console.log('no');
			} else {
				if (this.pickTeam.pickedElements.length < 3) {
					console.log('push');
					push(this);
				}
			}
		}

		console.log(this.pickTeam.pickedElements);


		function push(thisRef) {
			thisRef.pickTeam.pickedElements.push(event.srcElement);
			// thisRef.pickTeam.collection.push(item);
			// thisRef.pickTeam.selectedSpells[thisRef.pickTeam.pickedElements.length - 1].length = item.slots;
			
			event.srcElement.classList.add('added');

			// novo
			var index = 0;
			if (thisRef.pickTeam.team.length) {
				index = thisRef.pickTeam.team.length
			};
			var ph = thisRef.pickTeam.team;
			ph[index] = {};
			ph[index].character = {};
			ph[index].spells = [];

			ph[index].character = item;
		}

	}

	getSpell(item, event) {
		// novo
		const team = this.pickTeam.team;
		if(this.pickTeam.activeSlot > -1) {
			const actTeam = team[this.pickTeam.activeSlot];
			
			let length = 0;
			if(actTeam.spells) {
				length = actTeam.spells.length;
			} else {
				actTeam.spells = [];
			}
	
			if(actTeam.character.slots > length) {
				actTeam.spells.push({
					name: item.name
				});
			}
		}
	
	}

	getConsumable(id: string) {
		console.log(id);
	}

	onLogout() {
		this.authService.logout();
	}

	fakeChar() {
		const classes = [
			"priest",
			"druid",
			"warrior",
			"assassin",
			"mage",
			"warlock",
			"knight"
		];
		const rarities = [
			{
				name: "common",
				traits: 0
			},
			{
				name: "rare",
				traits: 1
			},
			{
				name: "epic",
				traits: 2
			},
			{
				name: "legendary 1",
				traits: 3
			},
			{
				name: "legendary 2",
				traits: 3
			},
			{
				name: "legendary 3",
				traits: 3
			},
			{
				name: "god",
				traits: 99
			},
		];
		const traits = ["0", "1", "2", "3", "4", "5", "6", "7", "8"];

		let object: any = {};
		let increment = -1;

		object.class = classes[this.packs.getRandom(classes.length)];
		
		for(let i = 0; i < rarities.length; i++) {
			var number = Math.floor((Math.random() * 100) + 1);
			if(number > 75) {
				increment++;
			} else {
				if(i === 0) {
					increment = 0;
				}
				break;
			}
		}


		object.rarity = rarities[increment].name;
		object.traits = [];

		if(increment === rarities.length - 1) {
			object.traits = traits;
			
		} else {
			for(let i = 0; i < rarities[increment].traits; i++) {
				var trait = traits[this.packs.getRandom(traits.length)];
				
				object.traits.push(trait);
			}			
		}

		console.log(object);
	}
}