import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

interface Profile {
	finishedTutorial: boolean,
	displayName: string,
	spellCollection: string[],
	characterCollection: string[],
	consumableCollection: string[],
	gold: number;
}

@Injectable()
export class ProfileService {
	profileDoc: AngularFirestoreDocument<Profile>
	profile: Observable<Profile>;
	
	constructor(
		private auth: AuthService,
		private afs: AngularFirestore
	) { }

	getUser() {
		console.log('getUser');
		if(this.auth.userId) {
			this.profileDoc = this.afs.doc('profiles/' + this.auth.userId);
			this.profile = this.profileDoc.valueChanges();
			this.profile.subscribe(data => {
				console.log(data);
			})
		}
	}

}
