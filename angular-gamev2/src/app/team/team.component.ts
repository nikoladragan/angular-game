import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';

@Component({
	selector: 'app-team',
	templateUrl: './team.component.html',
	styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

	constructor(
		private global: GlobalService
	) { }

	ngOnInit() {
	}

	onActivate(event) {
		this.global.teamChild = true;
	}
	onDeactivate(event) {
		this.global.teamChild = false;
	}
}
