import { Component, OnInit, Output } from '@angular/core';
import { GlobalService } from '../services/global.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	constructor(
		private global: GlobalService
	) { }

	ngOnInit() {
	}

}
