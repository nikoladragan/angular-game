import { Injectable } from '@angular/core';

@Injectable()
export class TeamService {
	team: any[] = [
		[
			{
				character: {
					desc: "desc 2",
					name: "character 2"
				},
				spells: [
					{
						name: "spell 10",
						desc: "desc 10"
					},
					{
						name: "spell 11",
						desc: "desc 11"
					},
					{
						name: "spell 7",
						desc: "desc 7"
					}
				],
				consumables: [
					{
						name: "consumable 8",
						desc: "desc 8"
					},
					{
						name: "consumable 11",
						desc: "desc 11"
					},
					{
						name: "consumable 5",
						desc: "desc 5"
					},
					{
						name: "consumable 4",
						desc: "desc 4"
					}
				]
			},
			{
				character: {
					desc: "desc 3",
					name: "character 3"
				},
				spells: [
					{
						name: "spell 10",
						desc: "desc 10"
					},
					{
						name: "spell 11",
						desc: "desc 11"
					},
					{
						name: "spell 7",
						desc: "desc 7"
					}
				],
				consumables: [
					{
						name: "consumable 8",
						desc: "desc 8"
					},
					{
						name: "consumable 11",
						desc: "desc 11"
					},
					{
						name: "consumable 5",
						desc: "desc 5"
					},
					{
						name: "consumable 4",
						desc: "desc 4"
					}
				]
			},
			{
				character: {
					desc: "desc 4",
					name: "character 4"
				},
				spells: [
					{
						name: "spell 10",
						desc: "desc 10"
					},
					{
						name: "spell 11",
						desc: "desc 11"
					},
					{
						name: "spell 7",
						desc: "desc 7"
					}
				],
				consumables: [
					{
						name: "consumable 8",
						desc: "desc 8"
					},
					{
						name: "consumable 11",
						desc: "desc 11"
					},
					{
						name: "consumable 5",
						desc: "desc 5"
					},
					{
						name: "consumable 4",
						desc: "desc 4"
					}
				]
			},
		]
	];
	constructor() { }

}
