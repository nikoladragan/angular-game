import { Injectable } from '@angular/core';

@Injectable()
export class CharactersService {
	characters: any[] = [
		{
			name: 'character 1',
			desc: 'desc 1',
		},
		{
			name: 'character 2',
			desc: 'desc 2',
		},
		{
			name: 'character 3',
			desc: 'desc 3',
		},
		{
			name: 'character 4',
			desc: 'desc 4',
		},
		{
			name: 'character 5',
			desc: 'desc 5',
		},
		{
			name: 'character 6',
			desc: 'desc 6',
		},
		{
			name: 'character 7',
			desc: 'desc 7',
		},
		{
			name: 'character 8',
			desc: 'desc 8',
		},
		{
			name: 'character 9',
			desc: 'desc 9',
		},
		{
			name: 'character 1',
			desc: 'desc 1',
		},
		{
			name: 'character 10',
			desc: 'desc 10',
		},
		{
			name: 'character 11',
			desc: 'desc 11',
		},
		{
			name: 'character 12',
			desc: 'desc 12',
		},
		{
			name: 'character 13',
			desc: 'desc 13',
		},
		{
			name: 'character 14',
			desc: 'desc 14',
		},
		{
			name: 'character 15',
			desc: 'desc 15',
		},
		{
			name: 'character 16',
			desc: 'desc 16',
		},
		{
			name: 'character 17',
			desc: 'desc 17',
		},
		{
			name: 'character 18',
			desc: 'desc 18',
		},
		{
			name: 'character 19',
			desc: 'desc 19',
		},
		{
			name: 'character 20',
			desc: 'desc 20',
		},
	]
	constructor() { }

}
