import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

interface Spell {
	amount: number,
	description: string,
	duration: number,
	name: string,
	spellGroup: string,
	type: string
}
interface Character {
	name: string,
	description: string,
	type: string
}
interface Consumable {
	name: string,
	description: string,
	type: string
}


@Component({
	selector: 'app-admin-panel',
	templateUrl: './admin-panel.component.html',
	styleUrls: ['./admin-panel.component.scss']
})

export class AdminPanelComponent implements OnInit {
	collection: AngularFirestoreCollection<any>;
	@Input() spellType: string;
	@Input() formType: string = "spells";

	constructor(
		private asf: AngularFirestore
	) { }

	ngOnInit() {
	}

	onSubmit(form: NgForm) {
		const f = form.value;
		var modal;
		var collectionName: string = this.formType;

		switch(this.formType) {
			case 'spells':
				modal = <Spell>{};
				this.collection = this.asf.collection<Spell>(collectionName);
				modal = f;
				f.type === 'overtime' ? modal.duration = f.duration : modal.duration = 1;
		
				break;
			case 'characters':
				modal = <Character>{};
				this.collection = this.asf.collection<Character>(collectionName);
				modal = f;
		
				break;
			case 'consumables':
				modal = <Consumable>{};
				this.collection = this.asf.collection<Consumable>(collectionName);
				modal = f;
				
				break;
			default:
				return null;
		}

		this.collection.add(modal);
		console.log(form.value);

		
		
		// this.collection = this.asf.collection<Spell>(collectionName);
		// var modal = <Spell>{};
		// modal.amount = f.amount;
		// modal.description = f.description;
		// f.type === 'overtime' ? modal.duration = f.duration : modal.duration = 1;
		// modal.name = f.name;
		// modal.spellGroup = f.spellGroup;
		// modal.type = f.type;

		// this.collection = this.asf.collection('spells');
		// this.collection.add(modal);
		
	}

	onSubmitCharacter(form: NgForm) {

	}

	onSubmitConsumable(form: NgForm) {

	}

}
