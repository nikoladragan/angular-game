import { PacksService } from './../services/packs.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { NgForm } from '@angular/forms';
import { Component, OnInit, OnChanges, IterableDiffers, SimpleChanges, DoCheck } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { AuthService } from '../services/auth.service';
import 'rxjs/Rx';

@Component({
	selector: 'app-tutorial',
	templateUrl: './tutorial.component.html',
	styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit, DoCheck {
	constructor(
		private Packs: PacksService,
	) {	}

	ngOnInit() {}

	ngDoCheck() {
		this.Packs.doCheck();
	}

	onSubmit(form: NgForm) {
		const newDisplayName = form.value.displayName;
		this.Packs.updateProfile({
			displayName: newDisplayName,
			finishedTutorial: true
		});
	}

	// getFirstPack(element) {
	// 	this.Packs.getFirstPack(element);
	// }
}
