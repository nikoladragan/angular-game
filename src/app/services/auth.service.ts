import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
// import { Profile } from './../profile/profile.model';


interface Profile {
	finishedTutorial: boolean,
	displayName: string,
	spellCollection: number[]
}


@Injectable()
export class AuthService {
	token: string;
	userId: string = null;
	profileCollection: AngularFirestoreCollection<Profile>;
	profiles: Observable<Profile[]>;
	isLoggedIn: boolean = false;
	newProfile = {
		displayName: '',
		finishedTutorial: false,
		spellCollection: []
	};
	// https://codepen.io/zmuci/pen/mBZybW
	normalString = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m", " "];
	shuffleString = ["C", "1", "v", "h", "m", "Z", "z", "6", "T", "Q", "9", "l", "J", "f", "8", "e", "d", "X", "M", "4", "L", "P", "n", "H", "r", "7", "u", "K", "F", "Y", "B", "W", "k", "G", "R", "E", "S", "2", "j", "0", "g", "a", "U", "O", "p", "o", "w", "N", "V", "i", "3", " ", "x", "q", "5", "I", "y", "A", "b", "t", "D", "s", "c"];

	constructor(
		private router: Router,
		private asf: AngularFirestore,
	) {
		firebase.auth().onAuthStateChanged(function (user) {
			if (user) {
				this.isLoggedIn = true;
				this.userId = firebase.auth().currentUser.uid;
				localStorage.setItem('id', this.userId);
			} else {
				this.isLoggedIn = false;
			}
		});
	}

	loginUser(email: string, password: string) {
		console.log(email, password);
		firebase.auth().signInWithEmailAndPassword(email, password).then(
			response => {
				this.router.navigate(['/profile']);
				firebase.auth().currentUser.getIdToken().then(
					(token: string) => this.token = token
				)
				this.userId = firebase.auth().currentUser.uid;
				localStorage.setItem('id', this.userId);
			}
		).catch(
			error => console.log(error)
			)
	}

	registerUser(email: string, password: string) {
		console.log(email, password);
		firebase.auth().createUserWithEmailAndPassword(email, password).then(
			response => {
				this.userId = response.uid;
				// ovde pozovi sve to
				// console.log(this.newProfile);
				this.newProfile.displayName = email;

				this.profileCollection = this.asf.collection('profiles');
				this.profiles = this.profileCollection.valueChanges();
				this.profileCollection.doc(this.userId).set(this.newProfile);

				this.loginUser(email, password);
			}
		).catch(
			error => console.log(error)
			)
	}

	logout() {
		firebase.auth().signOut();
		this.token = null;
		this.userId = null;
		localStorage.removeItem('id');
		this.router.navigate(['/login']);
	}

	isAuthenticated() {
		return this.token != null;
	}

	getToken() {
		// firebase.auth().currentUser.getIdToken().then(
		// 	(token: string) => {
		// 		return token
		// 	}
		// )
	}

	encode(string) {
		var a1 = this.normalString;
		var a2 = this.shuffleString;
		var a3 = string.split('');
		var a4 = [];

		for (var i = 0; i < a3.length; i++) {
			var pos = a1.indexOf(a3[i]);
			if (pos > -1) {
				a4.push(a2[pos]);
			} else {
				a4.push(a3[i]);
			}
		}

		return a4.join('');
	}

	decode(string) {
		var a1 = this.shuffleString;
		var a2 = this.normalString;
		var a3 = string.split('');
		var a4 = [];

		for (var i = 0; i < a3.length; i++) {
			var pos = a1.indexOf(a3[i]);
			if (pos > -1) {
				a4.push(a2[pos]);
			} else {
				a4.push(a3[i]);
			}
		}

		return a4.join('');
	}
}
